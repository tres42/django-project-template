# {{ project_name|title }} Django Project

## Prerequisites

* python >= 2.7
* pip
* virtualenv/wrapper (optional)


## Installation

### Creating the environment

Create a virtual python environment for the project.
If you're not using virtualenv or virtualenvwrapper you may skip this step.

#### For virtualenvwrapper

    :::bash
    mkvirtualenv {{ project_name }}

#### For virtualenv

    :::bash
    virtualenv {{ project_name }}
    cd {{ project_name }}
    source bin/activate


### Clone the code

Obtain the url to your git repository.

    :::bash
    git clone <URL_TO_GIT_RESPOSITORY> {{ project_name }}


### Install requirements

    :::bash
    cd {{ project_name }}
    pip install -r requirements/dev.txt # For development or
    pip install -r requirements/test.txt # For testing or
    pip install -r requirements.txt # For production


### Configure project

    :::bash
    cp {{ project_name }}/local_settings.dist.py {{ project_name }}/local_settings.py
    vim {{ project_name }}/local_settings.py # or editor of choice


### Sync database

    :::bash
    python manage.py syncdb
    python manage.py migrate # if using south as migration manager


## Running
    :::bash
    python manage.py runserver

Open browser to http://127.0.0.1:8000
